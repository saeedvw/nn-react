import React from 'react';
import { connect } from 'react-redux';

import {  eventActions } from '../_actions';

class EventPage extends React.Component {
    
    componentDidMount() {
        let { eventId } = this.props.match.params
        this.props.dispatch(eventActions.getEventById(eventId))        
    }
    
    render() {
        let { eventId } = this.props.match.params
        const {event} = this.props.events;
        return (
            <div >
                <p>EVENT PAGE {eventId}</p>
                <h4>{event && event.title}</h4>
                
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { authentication,events } = state;
    const { user } = authentication;
    return {
        user,
        events
    };
}

const connectedEventPage = connect(mapStateToProps)(EventPage);
export { connectedEventPage as EventPage };
