import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { eventActions, categoriesActions } from '../_actions';
import { CustomTextInput as TextInput, CustomCheckBox as CheckBox } from '../_components';
import Moment from 'react-moment';
import { CustomButton } from '../CustomButton';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import './style.css'

class EventsPage extends React.Component {
    state = {
        title: '',
        discr: '',
        date: '',
        special: false,
        limited: false,
        limit: undefined,
        allowedCategs: []
    }
    componentDidMount() {
        let { user, posted } = this.props;
        console.log(user._id);
        this.props.dispatch(eventActions.getCompanyEvent(user._id));
        this.props.dispatch(categoriesActions.getCompCategs(user._id));

    }
    handleChange = (e) => {
        const { name, value } = e.target;
        // console.log(e.target);
        if (name == 'special' || name == 'limited') {
            let valuee = !(this.state[name]);
            this.setState({ [name]: valuee })
            console.log(valuee)
        }
        this.setState({ [name]: value })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        const details = {
            title: this.state.title,
            discr: this.state.discr,
            date: new Date().toISOString(),
            allowedCategs: this.state.allowedCategs
        }
        console.log(JSON.stringify(details));
        this.props.dispatch(eventActions.addEvent(details))
        this.setState({ title: '', discr: '' });
    }
    handleCheckBoxCateg = (e) => {
        const { id, checked } = e.target;
        if (checked) {
            let newCategs = this.state.allowedCategs;
            newCategs.push(id);
            this.setState({ allowedCategs: newCategs });
        } else {
            let newCategs = this.state.allowedCategs.filter(c => c !== id);
            this.setState({ allowedCategs: newCategs });
        }
    }
    handleCheckBoxChange = (e) => {
        const { name, checked } = e.target;
        if (checked) {
            this.setState({ [name]: true })
        } else {
            this.setState({ [name]: false })
        }
    }
    handleDateChange = (date) => {
        this.setState({ date })
    }
    // <h4>Allowed Categories</h4>
    //                         <ul>
    //                             {categories.categories && categories.categories.map(categ => {
    //                                 return (
    //                                     <li key={categ._id}>{categ.title} <input onChange={this.handleCheckBoxCateg} id={categ._id} type='checkbox' /> </li>
    //                                 )
    //                             })}
    //                         </ul>


    render() {
        const { user, events, categories } = this.props;
        const { posting } = events;
        // console.log('users',users.items);
        return (
            <div className=''  >
                <div style={{display:'grid',placeItems:'center center',height:'100%' }}>
                    
                        <form onSubmit={this.handleSubmit} className='formBox'>
                        
                            <TextInput id='title' handleChange={this.handleChange} name='title' value={this.state.title} placeholder='Title' />
                            
                            <TextInput id='discr' handleChange={this.handleChange} name='discr' value={this.state.discr} placeholder='Discription' />

                            <DatePicker id='date' value={this.state.date} dateFormat='dd-MM-yyyy' selected={this.state.date} placeholderText='date' onChange={this.handleDateChange} customInput={<TextInput />} />



                            <CheckBox label='special'  name='special' checked={this.state.special} onChange={this.handleCheckBoxChange} />

                            <CheckBox label='limited' name='limited' checked={this.state.limited} onChange={this.handleCheckBoxChange} />



                            <TextInput id='limit' type='number' min='0' disabled={!this.state.limited} handleChange={this.handleChange} name='limit' value={this.state.limit} placeholder='limit' />
                            <input type='file'/>

                            <CustomButton id='add' text='add' />
                            {posting &&
                                <img id='loading' src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                            }
                        </form>
                    
                </div>

                {/* <div className='row'>
                    {events.events && events.events.map((event, n) => {
                        let date = new Date(event.date);
                        return (
                            <div key={event._id} className="card col-sm-3" >
                                <img className="card-img-top " />
                                <div className="card-body">
                                    <h5 className="card-title">{event.title}</h5>
                                    <p className="card-text">{date.toDateString()}</p>
                                    <Link to={`/events/event/${event._id}`} className="btn btn-primary">Show Details</Link>
                                </div>
                            </div>
                        )
                    })}
                </div> */}

            </div>
        );
    }
}

function mapStateToProps(state) {
    const { authentication, events, categories } = state;
    const { user } = authentication;
    // const {event}=events;
    return {
        user,
        events,
        categories
    };
}

const connectedEventsPage = connect(mapStateToProps)(EventsPage);
export { connectedEventsPage as EventsPage };
