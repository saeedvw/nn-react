import React, { Component } from 'react';
import 'react-dom'
import './navbar.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { Link , NavLink} from 'react-router-dom';

// import { Nav, Navbar, Form, Button, FormControl,NavDropdown } from 'react-bootstrap';
export class Navbar extends Component {
    state = {
        menuClass: 'menu__',
        navbarClass: 'navbar__ s',
        navClass: 'nav_'
    }


    handleToggle = () => {
        if (this.state.menuClass === 'menu__toggled') {
            this.setState({ menuClass: 'menu__', navbarClass: 'navbar__ s', navClass: 'nav_' })
        } else {
            this.setState({ menuClass: 'menu__toggled', navbarClass: 'navbar__toggled s', navClass: 'nav_toggled' })
        }
    }
    goHome = () => {
        window.location.href = '/';
    }
    render() {
        return (
            <nav className={this.state.navbarClass}>
                <div onClick={this.goHome} className='logo' ><span className='natalie'>NATALIE</span><span className='nights'>  nights</span></div>
                <FontAwesomeIcon onClick={this.handleToggle} className='navbar-toggle' icon='bars' />
                <ul className={this.state.menuClass}>
                    <li className={this.state.navClass} ><NavLink activeClassName='' to="/">Home</NavLink></li>
                    <li className={this.state.navClass} ><NavLink activeClassName='' to="/dashboard">Dashboard</NavLink></li>
                    {/* <li className={this.state.navClass} ><a href="events">Events</a></li> */}
                    {/* <li className={this.state.navClass} ><a href="items">Items</a></li> */}
                    <li className={this.state.navClass} ><NavLink activeClassName='' to="/reservations">settings</NavLink></li>
                </ul>

            </nav>
        );
    }
}

