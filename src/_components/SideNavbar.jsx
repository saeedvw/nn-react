import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { history } from '../_helpers'
import './sideNavbar.css'
import { connect } from 'react-redux'
import SideNav, {
  Toggle,
  Nav,
  NavItem,
  NavIcon,
  NavText
} from '@trendmicro/react-sidenav'

import '@trendmicro/react-sidenav/dist/react-sidenav.css'

class SideNavbar extends Component {
  state = {
    expanded: false,
    selected:''
  }
  handleToggle = () => {
    console.log('before', this.state.expanded)
    this.setState({ expanded: !this.state.expanded })
    console.log('after', this.state.expanded)
  }
  render () {
    return (
      <React.Fragment>
        {!this.props.user &&
        <>
        <div
          className='header'>
            <div className='logo'
          >
            <span className='natalie'>NATALIE</span>
            <span className='nights'> nights</span>
          </div>
          </div>
        
        </>

        }
        {this.props.user &&<>
        <div
          className={
            this.state.expanded === false ? 'header' : 'header header-expanded'
          }
        >
          <div
            onClick={() => {
              history.push('/')
            }}
            className='logo'
          >
            <span className='natalie'>NATALIE</span>
            <span className='nights'> nights</span>
          </div>
          <div className='userInfo'>
          <p className='username'><FontAwesomeIcon icon='user' style={{fontSize:'1em'}} /> {this.props.user &&(this.props.user.username + '/' +this.props.user.role) }</p>
          <p className='logout' onClick={()=>{history.push('/login')}}><FontAwesomeIcon icon='sign-out-alt' style={{fontSize:'1em'}}/>Logout</p>
          </div>
          <div className='breadCrumps'>
            <span>Events/Add event</span>
          </div>
        </div>
        
        <SideNav
          onSelect={selected => {
            const to = '/' + selected
            this.setState({selected});
            history.push(to);
          }}
        >
          <SideNav.Toggle onClick={this.handleToggle} />
          <SideNav.Nav defaultSelected=''>
            
          <NavItem active={this.state.selected===''} eventKey=''>
              <NavIcon>
                <FontAwesomeIcon
                  icon='home'
                  style={{ fontSize: '1.75em' }}
                />
              </NavIcon>
              <NavText>Home</NavText>
            </NavItem>

            <NavItem  active={this.state.selected==='dashboard'} eventKey='dashboard'>
              <NavIcon>
                <FontAwesomeIcon
                  icon='tachometer-alt'
                  style={{ fontSize: '1.75em' }}
                />
              </NavIcon>
              <NavText>Dashboard</NavText>
            </NavItem>

            {/*      */}

            <NavItem active={this.state.selected==='events'} eventKey='events'>
              <NavIcon>
                <FontAwesomeIcon
                  icon='calendar'
                  style={{ fontSize: '1.75em' }}
                />
              </NavIcon>
              <NavText>Events</NavText>
              <NavItem eventKey='events/'>
                <NavText>
                  <FontAwesomeIcon icon='calendar' /> All Events
                </NavText>
              </NavItem>
              <NavItem eventKey='events/addEvent'>
                <NavText>
                  <FontAwesomeIcon icon='plus' /> Add Event
                </NavText>
              </NavItem>
            </NavItem> 

            {/*      */}
            
            <NavItem active={this.state.selected==='items'} eventKey='items'>
              <NavIcon>
                <FontAwesomeIcon icon='star' style={{ fontSize: '1.75em' }} />
              </NavIcon>
              <NavText>Products</NavText>
              <NavItem eventKey='items/'>
                <NavText>
                  <FontAwesomeIcon icon='star' /> All Products
                </NavText>
              </NavItem>
              <NavItem eventKey='products/addProduct'>
                <NavText>
                  <FontAwesomeIcon icon='plus' /> Add Product
                </NavText>
              </NavItem>
            </NavItem>

            {/*      */}
            <NavItem active={this.state.selected==='reports'} eventKey='reports'>
              <NavIcon>
                <FontAwesomeIcon icon='file' style={{ fontSize: '1.75em' }} />
              </NavIcon>
              <NavText>Reports</NavText>
            </NavItem>
          </SideNav.Nav>
        </SideNav>
        </>}
      </React.Fragment>
    )
  }
}
function mapStateToProps (state) {
  const { authentication, } = state
  const { user } = authentication
  // const {event}=events;
  return {
    user
  }
}

const connectedSideNav = connect(mapStateToProps)(SideNavbar)
export { connectedSideNav as SideNavbar }
