import React, { Component } from 'react';
import 'react-dom'
import './style.css';
import { Nav, Navbar, Form, Button, FormControl,NavDropdown } from 'react-bootstrap';
export class Top extends Component {
    state = {}
    render() {
        return (
            <Navbar style={{backgroundColor:'#0e1935 !important'}} bg="dark" variant='dark' expand="lg">
                <Navbar.Brand href="#home" className='natalie' >NATALIE<span className='nights'>  nights</span></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto pull-right" style={{fontFamily:'Bangers',fontSize:'1.2em'}}>
                        <Nav.Link active href="#home" >Home</Nav.Link>
                        <Nav.Link href="#link">Link</Nav.Link>
                        <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

