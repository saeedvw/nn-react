import React, { Component } from 'react'

import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './cardStyle.css'
import {history} from '../_helpers';
export class DashboardButton extends Component {
  state = {}
  // link = React.createRef();
  render () {
    return (
      <div className='dashCard' onClick={()=>{history.push('/'+this.props.to)}}>
        <Link
        ref="link"
          to={'/' + this.props.to}
        //   style={{ width: this.props.width }}
          className=''
        >
          {this.props.title}
        </Link>
        <FontAwesomeIcon className='dashCardIcon' icon={this.props.icon}/>
      </div>
    )
  }
}
