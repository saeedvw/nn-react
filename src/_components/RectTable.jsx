import React, { Component } from 'react'
import { Rect, Text, Transformer } from 'react-konva'
import Konva from 'konva'
export class RectTable extends Component {
  state = {
    textX: this.props.x,
    textY: this.props.y,
    textWidth: 0,
    textHeight: 0
  }
  onDragMove = e => {
    const { x, y } = e.target.attrs
    const h = this.txtNode.getTextHeight()
    const w = this.txtNode.getTextWidth()
    this.setState({ textX: x, textY: y, textHeight: h, textWidth: w })
    this.props.onDragMove({ i: this.props.id, x, y })
  }

  componentDidMount () {
    const w = this.txtNode.getTextWidth()
    const h = this.txtNode.getTextHeight()
    this.setState({ textWidth: w, textHeight: h })
  }
  render () {
    return (
      <>
        <Rect
          id={this.props.id}
          x={this.props.x}
          y={this.props.y}
          width={this.props.width}
          height={this.props.height}
          fill={this.props.fill}
          stroke={this.props.stroke}
          strokeWidth={this.props.strokeWidth}
          draggable={this.props.draggable}
          dragBoundFunc={this.props.dragBoundFunc}
          onClick={this.props.onClick}
          onDragMove={this.onDragMove}
          onDragStart={this.props.select}
          // onDragEnd={this.props.unSelect}
          ref={node => {
            this.rectNode = node
          }}
        />
        <Text
          text={this.props.text}
          fontFamily='Bangers'
          align='center'
          fill='#ffffff'
          textAlign='center'
          x={this.state.textX + this.props.width / 2 - this.state.textWidth / 2}
          y={this.state.textY + (this.props.height - this.state.textHeight) / 2}
          fontSize={30}
          ref={node => {
            this.txtNode = node
          }}
          onClick={() => this.props.onClick({ target: this.rectNode })}
          // onDragMove={(e)=>(this.onDragMove({target:{attrs:{x:e.target.attrs.x-this.state.textWidth,y:e.target.attrs.y-this.state.textHeight}}}))}
          // draggable
        />
      </>
    )
  }
}
