import React, { Component } from 'react'

import { CustomButton as Btn } from '../CustomButton'
import { CustomTextInput as Input } from './CustomTextInput'

export class TableData extends Component {
  state = {}
  render () {
    return (
      <div style={{ ...style.div, height: this.props.height, width: '200px' }}>
        {this.props.table && (
          <p>
              X:{this.props.table.x}<br/>
              Y:{this.props.table.y}
            <Input onChange={this.props.onTxtChange} name='h' value={this.props.table.h} placeholder='height' type='number' min={0}/>
            <Input onChange={this.props.onTxtChange} name='w' value={this.props.table.w} placeholder='width' type='number' min={0}/>
            <Input onChange={this.props.chgLabel} name='label' value={this.props.table.txt} placeholder='label' />
          </p>
        )}
      </div>
    )
  }
}

const style = {
  div: {
    color: 'white',
    backgroundColor: 'rgba(0,0,0,0.8)',
    outlineWidth: '5px',
    outlineColor: '#ff00de',
    outlineStyle: 'solid',
    flex: '7',
    fontFamily: 'bangers',
    fontSize: '1.5em',
    textAlign: 'center'
  }
}
