import React, { Component } from 'react'

import './forms.css'
export class CustomTextInput extends Component {
  state = {
    value: ''
  }
  render () {
    return (
      <div className={this.props.className} style={{ position: 'relative', width: 'auto' }}>
        <input
          type={this.props.type}
          max={this.props.max}
          min={this.props.min}
          step={this.props.step}
          disabled={this.props.disabled}
          name={this.props.name}
          required = {this.props.required}
          className={this.props.required&&this.props.error? 'text_input error':'text_input'}
          onChange={this.props.handleChange || this.props.onChange}
          id={this.props.id}
          value={this.props.value || ''}
          onClick={this.props.onClick || null}
        />
        <span  className={this.props.required&&this.props.error? 'floating-label error-label':'floating-label'}>{this.props.placeholder || ''}</span>
      </div>
    )
  }
}
