import React, { Component } from 'react';

export class CustomCheckBox extends Component {
    state = {}
    render() {
        return (

            <div className='content'>
                <p id='p'>{this.props.label}</p>
            <div id='x' style={{ width: '25px', height: '25px', margin: '0.5em' ,position:'relative'}}>
                
                <label className="checkbox">
                    <input id={this.props.id} name={this.props.name} checked={this.props.checked} onChange={this.props.onChange} type="checkbox" />
                    <span className="checkmark"></span>
                </label>
            </div>

            </div>


        );
    }
}
