import React, { Component } from 'react';

export class SmallDashboard extends Component {
    state = {  }
    render() { 
        return ( 
            <div className='smallDashboard'>
                {this.props.children}
            </div>
         );
    }
}
 