import React from 'react';
import { connect } from 'react-redux';

import {  itemsActions } from '../_actions';

class ItemsPage extends React.Component {
    state={
        name:'',
        price:'',
    }
    componentDidMount() {
        const {user} = this.props;
        this.props.dispatch(itemsActions.getCompItems(user._id));
        
    }
    handleChange=(e)=>{
        const {name,value}=e.target;
        this.setState({[name]:value});
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        const details ={
            name:this.state.name,
            price:this.state.price,
        };
        this.props.dispatch(itemsActions.addItem(details));

        this.setState({name:'',price:''});
    }
    render() {
        const {items} = this.props;
        return (
            <div >
                <p>ITEMS PAGE</p>
                <form onSubmit={this.handleSubmit}>
                <label htmlFor="name">Name</label>
                        <input type="text" className='form-control' onChange={this.handleChange} value={this.state.name} name="name" id="name" placeholder='name'/>
                    <label htmlFor="discr">Price</label>
                        <input type="number" className='form-control' onChange={this.handleChange} value={this.state.price} name="price" id="price" placeholder='price'/>
                   <br/>
                    <input type="submit" value='Add Item' className='btn btn-success'/>

                </form>

                <div>
                {items.items&&items.items.map((item,n)=>{
                       return(
                        <div key={item._id} className="card col-sm-3" >
                        <img alt={item.name}  className="card-img-top " />
                        <div className="card-body">
                          <h4 className="card-title">{item.name}</h4>
                          <h5>Price : {item.price}$</h5>
                        </div>
                      </div>
                       )
                   })}
                </div>
                
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { authentication,items } = state;
    const { user } = authentication;
    // const {event}=events;
    return {
        user,
        items
    };
}

const connectedItemsPage = connect(mapStateToProps)(ItemsPage);
export { connectedItemsPage as ItemsPage };
