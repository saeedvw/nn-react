// import config from 'config';
import { authHeader } from '../_helpers';
const location = window.location;
export const itemsService={
    getCompItems,
    addItem
};

const config={
    apiUrl:'http://localhost:5000'
};

function getCompItems(id){
    const requestOptions = {
        method: 'GET'
    };

    return fetch(`${config.apiUrl}/items/company/${id}`, requestOptions).then(handleResponse);
}

function addItem(details){
    const requestOptions={
        method:'POST',
        headers:{'Content-Type':'application/json','Authorization':authHeader()['Authorization']},
        body:JSON.stringify(details),
    };
    console.log(requestOptions);
    return fetch(`${config.apiUrl}/items`,requestOptions).then(handleResponse);
}





function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                // logout();
                location.reload(true);
            }
            console.log(text);
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}
