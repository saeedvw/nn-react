// import config from 'config';
import { authHeader } from '../_helpers';
const location = window.location;

export const eventsService={
    getCompanyEvents,
    addEvent,
    getEventById
} 

const config={
    apiUrl:'http://localhost:5000'
};

function getCompanyEvents(id){
    const requestOptions = {
        method: 'GET'
    };
    return fetch(`${config.apiUrl}/events/${id}`, requestOptions)
    .then(handleResponse);
}

function addEvent(details){
    const requestOptions={
        method:'POST',
        headers:{ 'Content-Type': 'application/json' ,'Authorization':authHeader()['Authorization']},
        body:JSON.stringify(details)
    };
    console.log(requestOptions);
    return fetch(`${config.apiUrl}/events`,requestOptions)
    .then(handleResponse);
}

function getEventById(id){
    const requestOptions={
        method:'GET',
    }
    return fetch(`${config.apiUrl}/events/event/${id}`,requestOptions)
    .then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                // logout();
                location.reload(true);
            }
            console.log(text);
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}