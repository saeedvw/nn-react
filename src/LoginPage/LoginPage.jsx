import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import {alertActions } from '../_actions';
import { userActions } from '../_actions'
import { Navbar } from '../_components'
import { CustomTextInput as TextInput } from '../_components'
// import SweetAlert from 'sweetalert2-rect'
import SweetAlert from 'react-bootstrap-sweetalert'
import { CustomButton } from '../CustomButton'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import {
  withGoogleMap,
  GoogleMap,
  Marker,
  withScriptjs
} from 'react-google-maps'
// import withScriptjs from 'react-google-maps/lib/async/withScriptjs';
import './loginPage.css'

const styles = require('./googleMapsStyle.json');
const MyMap = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap defaultZoom={8} defaultCenter={{ lat: -34.397, lng: 150.644 }} defaultOptions={{styles:styles}}>
      <Marker position={{ lat: -34.397, lng: 150.644 }} />
    </GoogleMap>
  ))
)

class LoginPage extends React.Component {
  constructor (props) {
    super(props)

    // reset login status
    this.props.dispatch(userActions.logout())

    this.state = {
      username: '',
      password: '',
      submitted: false,
      show: false,
      alertMessage: '',
      alertTitle: '',
      alertShow: this.props.alert.message !==''
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange (e) {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  handleSubmit (e) {
    e.preventDefault()
    this.setState({ submitted: true })
    const { username, password } = this.state
    const { dispatch } = this.props

    if (username && password) {
      dispatch(userActions.login(username, password))
    } else {
      if (!username) {
        this.setState({
          alertTitle: 'Username is required',
          alertMessage: 'Please enter your username'
        })
      }
      if (!password) {
        this.setState({
          alertTitle: 'Password is required',
          alertMessage: 'Please enter your password'
        })
      }
      if (!username && !password) {
        this.setState({
          alertTitle: 'Username and Password are required',
          alertMessage: 'Please enter your username and password'
        })
      }
      this.setState({ show: true })
    }
  }

  render () {
    const { loggingIn } = this.props
    const { username, password, submitted } = this.state

    return (
      <div className='loginPage'>
        {/* {this.props.alert && (
          <SweetAlert
          danger
          style={{
            backgroundColor: '#17284f',
            fontFamily: 'bangers',
            borderRadius: 0,
            border: '5px solid #ff00de'
          }}
            show={this.state.alertMessage}
            title={alert.message}
            onConfirm={() => {
              console.log('hii')
              this.props.dispatch(alertActions.clear());
              this.setState({ alertShow: false })
              
            }}
          >{this.props.alert.message}</SweetAlert>
        )} */}

        <SweetAlert
          danger
          style={{
            backgroundColor: '#17284f',
            fontFamily: 'bangers',
            borderRadius: 0,
            border: '5px solid #ff00de'
          }}
          show={this.state.show}
          title={this.state.alertTitle}
          confirmBtnbBsStyle='danger'
          text={this.state.alertMessage}
          onConfirm={() => {
            this.setState({ show: false });
          }}
        >
          {this.state.alertMessage}
        </SweetAlert>
        <div className='forms'>
          <form className='loginForm' name='form' onSubmit={this.handleSubmit}>
            <TextInput
            className='username_'
              type='text'
              
              error={submitted && !username}
              name='username'
              value={username}
              onChange={this.handleChange}
              placeholder='username'
            />

            <TextInput
              required
              className='password_'
              error={submitted && !password}
              type='password'
              name='password'
              value={password}
              onChange={this.handleChange}
              placeholder='password'
            />

            <CustomButton className='login__' text='Login' />
            {loggingIn && (
              <img
                alt='...'
                src='data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA=='
              />
            )}
            <Link to='/register' className='register__'>
              Don't have an account ?
            </Link>
            <Link to='/register' className='forggotPassword'>
              forggot password ?
            </Link>
          </form>
        </div>
        <div className='footer'>
          <div className='top'>
            <h4>Contact Us</h4>
            <h5>Phone +905338809529</h5>
            <h5>Email qadah9548@gmail.com</h5>
            <h5>Adress Uzun27 magusa Cyprus</h5>
            <h6>We are happy to do busines with you !</h6>
          </div>
          <div className='bottom'>
            <FontAwesomeIcon
              icon={['fab', 'facebook']}
              className='footer-icon'
            />
            <FontAwesomeIcon
              icon={['fab', 'instagram']}
              className='footer-icon'
            />
            <FontAwesomeIcon
              icon={['fab', 'snapchat']}
              className='footer-icon'
            />
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  const { loggingIn } = state.authentication
  const { alert } = state
  return {
    loggingIn,
    alert
  }
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage)
export { connectedLoginPage as LoginPage }
