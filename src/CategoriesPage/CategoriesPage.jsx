import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions, categoriesActions,itemsActions } from '../_actions';

class CategoriesPage extends React.Component {
    state={
        title:'',
        price:'',
        items:[]
    }
    componentDidMount() {
        const { user } = this.props;
        this.props.dispatch(categoriesActions.getCompCategs(user._id));
        this.props.dispatch(itemsActions.getCompItems(user._id));

    }
    handleChange=(e)=>{
        const {name,value}=e.target;
        this.setState({[name]:value});
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        const details ={
            title:this.state.title,
            price:this.state.price,
            items:this.state.items

        };
        this.props.dispatch(categoriesActions.addCateg(details));


        this.setState({title:'',price:''});
    }
    handleCheckBox=e=>{
        const {checked} = e.target;
        if(checked){
            let newItems = [...this.state.items,e.target.id];
            // console.log(this.state);
            this.setState({items:newItems});
            // console.log(this.state);
        }
        else{
            
            let newItems = this.state.items.filter(v=>{return v!==e.target.id});
            // newItems.remove
            this.setState({items:newItems})
        }
    }


    render() {
        const categs = this.props.categories.categories;
        const items = this.props.items.items;
        return (
            <div >
                <p>CATEGORIES PAGE</p>
                <form onSubmit={this.handleSubmit}>
                            <label htmlFor="title">Title</label>
                            <input type="text" className='form-control' onChange={this.handleChange} value={this.state.title} name="title" id="title" placeholder='title' />
                            <label htmlFor="discr">Price</label>
                            <input type="number" className='form-control' onChange={this.handleChange} value={this.state.price} name="price" id="price" placeholder='price' />
                            <br />
                            <h3>Items :</h3>
                            {items&&items.map(item=>{
                                return(
                                    <div key={item._id} className='col-sm-4'>
                                    <label htmlFor={`${item._id}`}>
                                        {item.name}
                                    </label>
                                    <input onChange={this.handleCheckBox} id={`${item._id}`} type='checkbox'/>
                                </div>
                                )
                            })}
                            <br/><br/><br/>
                            <input type="submit" value='Add Categorie' className='btn btn-success' />
                        </form>
                <div><br/><br/><hr/>
                    {categs && categs.map((categ, n) => {
                       
                        return (
                            <div key={categ._id} className="card col-sm-3" >
                                <img className="card-img-top " />
                                <div className="card-body">
                                    <h4 className="card-title">{categ.title}</h4>
                                    <h5>Price : {categ.price}$</h5>
                                    <div>
                                        <ul>{categ.items.map(item=><li key={item._id} className='text-muted' >{item.name}</li>)}</ul>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </div>

            </div>
        );
    }
}

function mapStateToProps(state) {
    const { authentication, items, categories } = state;
    const { user } = authentication;
    // const {event}=events;
    return {
        user,
        items,
        categories
    };
}

const connectedCategoriesPage = connect(mapStateToProps)(CategoriesPage);
export { connectedCategoriesPage as CategoriesPage };
