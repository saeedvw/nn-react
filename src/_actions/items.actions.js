import { itemsConstants } from '../_constants';
import { itemsService } from '../_services';
import { alertActions } from './';
// import { history } from '../_helpers';

export const itemsActions = {
    getCompItems,
    addItem
}

function getCompItems(id) {
    return dispatch => {
        dispatch(request({ id }));

        itemsService.getCompItems(id)
            .then(
                items => { 
                    dispatch(success(items));
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            );
    };

    function request(id) { return { type: itemsConstants.GET_ITEMS_REQUEST, id } }
    function success(items) { return { type: itemsConstants.GET_ITEMS_REQUEST_SUCCESS, items } }
    function failure(items) { return { type: itemsConstants.GET_ITEMS_REQUEST_ERROR, items} } 
}

function addItem(details){
    return dispatch=>{
        dispatch(request({ details }));

        itemsService.addItem(details)
        .then(
            item=>{
                dispatch(success(item));
            },
            error=>{
                dispatch(failure(error.toString()));
            }
        )
    };

    function request(details){return {type:itemsConstants.POST_ITEM_REQUEST,details}}
    function success(item){return{type:itemsConstants.POST_ITEM_REQUEST_SUCCESS,item}}
    function failure(item){return{type:itemsConstants.POST_ITEM_REQUEST_ERROR,item}}

}