import { eventConstants } from '../_constants';
import { eventsService } from '../_services';
import { alertActions } from './';
// import { history } from '../_helpers';

export const eventActions = {
    getCompanyEvent,
    addEvent,
    getEventById
}

function getCompanyEvent(id) {
    return dispatch => {
        dispatch(request({ id }));

        eventsService.getCompanyEvents(id)
            .then(
                events => { 
                    dispatch(success(events));
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            );
    };

    function request(id) { return { type: eventConstants.GET_REQUEST, id } }
    function success(events) { return { type: eventConstants.GET_REQUEST_SUCCESS, events } }
    function failure(events) { return { type: eventConstants.GET_REQUEST_ERROR, events} } 
}
function addEvent(details) {
    return dispatch => {
        dispatch(request({ details }));

        eventsService.addEvent(details)
            .then(
                result => { 
                    dispatch(success(result));
                    dispatch(alertActions.success('New event was created'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()))
                }
            );
    };

    function request(details) { return { type: eventConstants.POST_REQUEST, details } }
    function success(event) { return { type: eventConstants.POST_REQUEST_SUCCESS, event } }
    function failure(event) { return { type: eventConstants.POST_REQUEST_ERROR, event} } 
}
function getEventById(id) {
    return dispatch => {
        dispatch(request({ id }));

        eventsService.getEventById(id)
            .then(
                event => { 
                    dispatch(success(event));
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            );
    };

    function request(id) { return { type: eventConstants.GET_REQUEST, id ,one:true} }
    function success(events) { return { type: eventConstants.GET_REQUEST_SUCCESS, events,one:true } }
    function failure(events) { return { type: eventConstants.GET_REQUEST_ERROR, events,one:true} } 
}