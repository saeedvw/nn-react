import { categoriesConstants } from '../_constants';
import { categoriesService } from '../_services';
import { alertActions } from './';
// import { history } from '../_helpers';

export const categoriesActions = {
    getCompCategs,
    addCateg
}

function getCompCategs(id) {
    return dispatch => {
        dispatch(request({ id }));

        categoriesService.getCompCategs(id)
            .then(
                categs => { 
                    dispatch(success(categs));
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            );
    };

    function request(id) { return { type: categoriesConstants.GET_CATEGS_REQUEST, id } }
    function success(categories) { return { type: categoriesConstants.GET_CATEGS_REQUEST_SUCCESS, categories } }
    function failure(categories) { return { type: categoriesConstants.GET_CATEGS_REQUEST_ERROR, categories} } 
}

function addCateg(details){
    return dispatch=>{
        dispatch(request({ details }));

        categoriesService.addCateg(details)
        .then(
            categ=>{
                dispatch(success(categ));
            },
            error=>{
                dispatch(failure(error.toString()));
            }
        )
    };

    function request(details){return {type:categoriesConstants.POST_CATEG_REQUEST,details}}
    function success(categorie){return{type:categoriesConstants.POST_CATEG_REQUEST_SUCCESS,categorie}}
    function failure(categorie){return{type:categoriesConstants.POST_CATEG_REQUEST_ERROR,categorie}}

}