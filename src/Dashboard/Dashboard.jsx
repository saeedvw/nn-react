import React, { Component } from 'react';


import {DashboardButton as Card } from '../_components';

export class Dashboard extends Component {
    state = {  }
    render() { 
        return ( 
            <div style={{textAlign:'center'}} className='container'><br/>
            <h1 style={{fontFamily:'Bangers',textAlign:'center'}}>Dashboard</h1><br/>
            <div className='row'>
                <Card to='events' icon='calendar' title='Events' width='200px'/>
                <Card to='categories' icon='list-alt' title='Categories' width='200px'/>
                <Card to='staff' icon='users' title='staff' width='200px'/> 
                <Card to='items' icon='star' title='Items' width='200px'/>
                <Card to='tables' icon='box' title='tables' width='200px'/>
                <Card to='reports' icon='file' title='reports' width='200px'/>
                <Card title='reports' width='200px'/>
                </div>
            </div>
         );
    }
}