import React from 'react'
import { Router, Route, BrowserRouter, Switch } from 'react-router-dom'
import { connect } from 'react-redux'

import { history } from '../_helpers'
import { alertActions } from '../_actions'
import { PrivateRoute } from '../_components'
import { HomePage } from '../HomePage'
import { LoginPage } from '../LoginPage'
import { RegisterPage } from '../RegisterPage'
import { EventsPage } from '../EventsPage'
import { EventPage } from '../EventPage'
import { ItemsPage } from '../ItemsPage'
import { CategoriesPage } from '../CategoriesPage'
import { Dashboard } from '../Dashboard'
import { TablesPage } from '../TablesPage'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook,faInstagram,faSnapchat } from '@fortawesome/free-brands-svg-icons' 
import {
  faBars,
  faUsers,
  faFile,
  faStar,
  faBox,
  faCalendar,
  faListAlt,
  faHome,
  faPlus,
  faTachometerAlt,
  faUser,
  faSignOutAlt,
  
} from '@fortawesome/free-solid-svg-icons'
import SweetAlert from 'react-bootstrap-sweetalert'

import { Navbar,SideNavbar } from '../_components'
library.add(faSignOutAlt)
library.add(faUser);
library.add(faBars)
library.add(faUsers)
library.add(faFile)
library.add(faStar)
library.add(faBox)
library.add(faCalendar)
library.add(faListAlt)
library.add(faHome)
library.add(faPlus)
library.add(faTachometerAlt)
library.add(faFacebook)
library.add(faInstagram);
library.add(faSnapchat)
class App extends React.Component {
  constructor (props) {
    super(props)

    const { dispatch } = this.props
    history.listen((location, action) => {
      // clear alert on location change
      dispatch(alertActions.clear())
    })
  }
  state = {
    show: false
  }

  render () {
    const { alert } = this.props
    return (
      <div>
        <Router history={history}>
          <div>
            {/* <Navbar /> */}
            <SideNavbar/>
            {alert.message && (
              <SweetAlert
                {...(alert.type === 'success'
                  ? (this.success = true)
                  : (this.danger = true))}
                style={{
                  backgroundColor: '#17284f',
                  fontFamily: 'bangers',
                  borderRadius: 0,
                  border: '5px solid #ff00de'
                }}
                show
                title={this.state.alertTitle}
                confirmBtnbBsStyle='danger'
                text={this.state.alertMessage}
                onConfirm={() => {
                  this.setState({ show: false })
                  this.props.dispatch(alertActions.clear())
                }}
              >
                {alert.message}
              </SweetAlert>
            )}

            <Switch>
              <PrivateRoute exact path='/' component={HomePage} />
              <PrivateRoute exact path='/events' component={EventsPage} />
              <PrivateRoute
                exact
                path='/events/event/:eventId'
                component={EventPage}
              />
              <PrivateRoute exact path='/items' component={ItemsPage} />
              <PrivateRoute exact path='/dashboard' component={Dashboard} />
              <PrivateRoute
                exact
                path='/categories'
                component={CategoriesPage}
              />
              <PrivateRoute exact path='/tables' component={TablesPage} />
              <Route path='/login' component={LoginPage} />
              <Route path='/register' component={RegisterPage} />
            </Switch>
          </div>
        </Router>
      </div>
    )
  }
}

function mapStateToProps (state) {
  const { alert } = state
  return {
    alert
  }
}

const connectedApp = connect(mapStateToProps)(App)
export { connectedApp as App }
