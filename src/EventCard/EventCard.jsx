import React, { Component } from 'react';
import './eventStyle.css';

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTicketAlt,faCalendarDay,faUsers } from '@fortawesome/free-solid-svg-icons'


import {CustomButton as Btn} from '../CustomButton';

export class EventCard extends Component {
    state = {}
    render() {
        return ( 
            
            <div className='eCard'>
            <div className='compBox'>
                <h4 className='compName'>Thrones Club</h4>
            </div>
                <div className='imageBox'>
                    <img className='image' src={require('./img.png')} alt=""/>
                </div>
                <div className='info'>
                    <h4 className='title'>Halloween Party</h4>
                    <p className='discr'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem ad necessitatibus</p>
                </div>
                <div className='details' style={{textAlign:'justify'}}>
                    <span><FontAwesomeIcon className='icon' icon={faCalendarDay}/>&nbsp;Date&nbsp;&nbsp;&nbsp;</span>12/7/2019<br/>
                    <span><FontAwesomeIcon className='icon' icon={faTicketAlt}/>&nbsp;Entery&nbsp;&nbsp;&nbsp;</span>30$<br/>
                    <span><FontAwesomeIcon className='icon' icon={faUsers}/>&nbsp;Limit&nbsp;&nbsp;&nbsp;</span>150<br/>                    
                </div>
                <div className='rsvBtn'>
                    <Btn text='Reserve'/>
                </div>
            </div>
         );
    }
}