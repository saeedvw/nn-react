import React, { Component } from 'react'
import { Stage, Layer } from 'react-konva'
import Konva from 'konva'

import { RectTable, TableData } from '../_components'
import { CustomButton as Btn } from '../CustomButton'
export class TablesPage extends Component {
  state = {
    stage: {
      w: 700,
      h: 700
    },
    newH: 200,
    newW: 220,

    tables: [
      {
        id: 0,
        w: 120,
        h: 70,
        f: '#0e1935',
        s: '#ff00de',
        x: 20,
        y: 20,
        txt: 0
      },
      {
        id: 1,
        w: 120,
        h: 70,
        f: '#0e1935',
        s: '#ff00de',
        x: 160,
        y: 20,
        txt: 1
      },
      {
        id: 2,
        w: 120,
        h: 70,
        f: '#0e1935',
        s: '#ff00de',
        x: 300,
        y: 20,
        txt: 2
      },
      {
        id: 3,
        w: 120,
        h: 70,
        f: '#0e1935',
        s: '#ff00de',
        x: 440,
        y: 20,
        txt: 3
      },
      {
        id: 4,
        w: 120,
        h: 70,
        f: '#0e1935',
        s: '#ff00de',
        x: 580,
        y: 20,
        txt: 4
      },
      {
        id: 5,
        w: 120,
        h: 70,
        f: '#0e1935',
        s: '#ff00de',
        x: 20,
        y: 110,
        txt: 5
      },
      {
        id: 6,
        w: 120,
        h: 70,
        f: '#0e1935',
        s: '#ff00de',
        x: 160,
        y: 110,
        txt: 6
      },
      {
        id: 7,
        w: 120,
        h: 70,
        f: '#0e1935',
        s: '#ff00de',
        x: 300,
        y: 110,
        txt: 7
      },
      {
        id: 8,
        w: 120,
        h: 70,
        f: '#0e1935',
        s: '#ff00de',
        x: 440,
        y: 110,
        txt: 8
      }
    ],
    selected: null
  }
  handleClick = e => {
    let i = e.target.attrs.id

    if (this.state.selected === i) {
      this.setState({ selected: null })
    } else {
      this.setState({ selected: i })
    }
  }
  dragBounds = (pos, i) => {
    let x
    let y
    const stageH = this.state.stage.h
    const stageW = this.state.stage.w
    const tableW = this.state.tables[i].w
    const tableH = this.state.tables[i].h
    if (pos.x === 0 || pos.x <= 0) {
      x = 0
    } else if (pos.x > stageW - tableW || pos.x === stageW) {
      x = stageW - tableW
    } else {
      x = pos.x
    }
    if (pos.y === 0 || pos.y <= 0) {
      y = 0
    } else if (pos.y >= stageH - tableH) {
      y = stageH - tableH
    } else {
      y = pos.y
    }
    // y=pos.y;
    return {
      x,
      y
    }
  }
  addTable = () => {
    let tables = this.state.tables
    tables.push({
      id: 9,
      w: 120,
      h: 70,
      f: '#0e1935',
      s: '#ff00de',
      x: 580,
      y: 110
    })
    this.setState({ tables })
  }
  delTable = () => {
    if (this.state.selected >= 0 && this.state.selected !== null) {
      let tables = this.state.tables
      delete tables[this.state.selected]
      this.setState({ tables })
    }
  }
  bigger = () => {
    if (this.state.selected >= 0 && this.state.selected !== null) {
      let tables = this.state.tables
      tables[this.state.selected].h += 20
      tables[this.state.selected].w += 20

      this.setState({ tables })
    }
  }
  smaller = () => {
    if (this.state.selected >= 0 && this.state.selected !== null) {
      let tables = this.state.tables
      tables[this.state.selected].h -= 20
      tables[this.state.selected].w -= 20

      this.setState({ tables })
    }
  }
  changeSize = e => {
    if (this.state.selected >= 0 && this.state.selected !== null) {
      let tables = this.state.tables
      const { name, value } = e.target
      tables[this.state.selected][name] = value
      // tables[this.state.selected].w = this.state.newW;

      this.setState({ tables })
    }
  }
  changeLabel = e => {
    if (this.state.selected >= 0 && this.state.selected !== null) {
      let tables = this.state.tables
      const { value } = e.target
      tables[this.state.selected].txt = value
      // tables[this.state.selected].w = this.state.newW;

      this.setState({ tables })
    }
  }
  handleTextChange = e => {
    const { name, value } = e.target
    this.setState({ form: { [name]: value } })
  }
  keyDown = e => {
    console.log(e)
  }
  handleDrag = e => {
    let tables = this.state.tables
    tables.map(table => {
      if (table.id === e.i) {
        table.x = e.x
        table.y = e.y
      }
      return table;
    });
    this.setState({ tables })
  }
  render () {
    return (
      <div
        onKeyDown={this.keyDown}
        style={style.container}
        className='container'
      >
        <TableData
          chgLabel={this.changeLabel}
          onTxtChange={this.changeSize}
          table={
            this.state.selected !== null
              ? this.state.tables[this.state.selected]
              : null
          }
          height={this.state.stage.h + 'px'}
        />
        <div
          style={{
            height: this.state.stage.h,
            width: this.state.stage.w,
            flex: 12
          }}
        >
          <Stage
            style={{
              backgroundColor: 'rgba(0,0,0,0.8)',
              outlineWidth: '5px',
              outlineColor: '#ff00de',
              outlineStyle: 'solid'
            }}
            width={this.state.stage.w}
            height={this.state.stage.h}
          >
            <Layer>
              {this.state.tables.map((t, i) => {
                let stroke = this.state.selected === i ? 'white' : '#ff00de'
                return (
                  <RectTable
                    id={i}
                    x={t.x}
                    y={t.y}
                    width={t.w}
                    height={t.h}
                    fill={t.f}
                    stroke={stroke}
                    strokeWidth={5}
                    draggable
                    onClick={this.handleClick}
                    onDragMove={this.handleDrag}
                    select={() => {
                      this.setState({ selected: i })
                    }}
                    unSelect={() => {
                      this.setState({ selected: null })
                    }}
                    dragBoundFunc={pos => this.dragBounds(pos, i)}
                    text={t.txt || String(i)}
                  />
                )
              })}
            </Layer>
          </Stage>
        </div>
      </div>
    )
  }
}

const style = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  }
}
