import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import { userActions, eventActions } from '../_actions'

import { EventCard } from '../EventCard'
import { SmallDashboard,  } from '../_components'
class HomePage extends React.Component {
  componentDidMount () {
    let { user } = this.props
    console.log(user._id)
    this.props.dispatch(eventActions.getCompanyEvent(user._id))
  }

  handleDeleteUser (id) {
    return e => this.props.dispatch(userActions.delete(id))
  }

  render () {
    const { user, events } = this.props
    // console.log('users',users.items);
    return (
      <div className='container'>
        
        <div
          className='row'
          style={{
            alignContent: 'center',
            justifyContent: 'center',
            textAlign: 'center',
            fontFamily: 'bangers',
            marginTop: '2em'
          }}
        >
          <h1 style={{ letterSpacing: '4px' }}>Upcoming Events</h1>
        </div>
        <div
          className='row'
          style={{ alignContent: 'center', justifyContent: 'center' }}
        >
          <EventCard />
          <EventCard special />
          <EventCard />
        </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  const { authentication, events } = state
  const { user } = authentication
  // const {event}=events;
  return {
    user,
    events
  }
}

const connectedHomePage = connect(mapStateToProps)(HomePage)
export { connectedHomePage as HomePage }
