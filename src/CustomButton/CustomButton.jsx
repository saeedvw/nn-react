import React, { Component } from 'react'

import './customBtn.css'

export class CustomButton extends Component {
  state = {}
  render () {
    return (
      <button onClick={this.props.onClick} className={'butn ' + this.props.className}>
        {this.props.text || 'Button'}
      </button>
    )
  }
}
