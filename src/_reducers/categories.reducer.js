import { categoriesConstants } from '../_constants';

export function categories(state = {}, action) {
    switch (action.type) {
        case categoriesConstants.GET_CATEGS_REQUEST:
            return { loading: true };
        case categoriesConstants.GET_CATEGS_REQUEST_SUCCESS:
            return { categories: action.categories };
        case categoriesConstants.GET_CATEGS_REQUEST_ERROR:
            return {};
        case categoriesConstants.POST_CATEG_REQUEST:
            return { ...state, posting: true };
        case categoriesConstants.POST_CATEG_REQUEST_SUCCESS:
            return { categories: [...state.categories, action.categorie] };
        case categoriesConstants.POST_CATEG_REQUEST_ERROR:
            return { categories: state.categories, error: true };
        default:
            return state;
    }
}