import { eventConstants } from '../_constants';

export function events(state={},action){
    switch (action.type) {
        case eventConstants.GET_REQUEST:
        if(action.one){
          return {
            gettingEvent: true,
            event: action.events
          }}
          return {
            gettingEvent: true,
            events: action.events
          }

        case eventConstants.GET_REQUEST_SUCCESS:
        if(action.one){
          return {
            event: action.events
          };
        }
        return{
          events:action.events
        }
        case eventConstants.GET_REQUEST_ERROR:
          return state;
        case eventConstants.POST_REQUEST:
          return {...state,posting:true}
          // return {events:[...state.events],posting:true,details:action.details};
        case eventConstants.POST_REQUEST_SUCCESS:
          let newEvent = action.event;    
          return {events:[...state.events,newEvent],posted:true};
        case eventConstants.POST_REQUEST_ERROR:
          return {events:state.events,error:true};
          // return {}

        default:
          return state
      }

}