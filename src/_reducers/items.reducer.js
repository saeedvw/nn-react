import { itemsConstants } from '../_constants';

export function items(state = {}, action) {
    switch (action.type) {
        case itemsConstants.GET_ITEMS_REQUEST:
            return { loading: true }
        case itemsConstants.GET_ITEMS_REQUEST_SUCCESS:
            return { items: action.items };
        case itemsConstants.GET_ITEMS_REQUEST_ERROR:
            return { error: true };
        case itemsConstants.POST_ITEM_REQUEST:
            return {...state,posting:true};
        case itemsConstants.POST_ITEM_REQUEST_SUCCESS:
            return {items:[...state.items,action.item]};
        case itemsConstants.POST_ITEM_REQUEST_ERROR:
            return {...state,posting:false,error:true};
        default:
            return state;
    }

}