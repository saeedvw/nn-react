import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { events } from './events.reducer';
import { items } from './items.reducer';
import { categories } from './categories.reducer';

const rootReducer = combineReducers({
  authentication,
  registration,
  users,
  alert,
  events,
  items,
  categories
});

export default rootReducer;